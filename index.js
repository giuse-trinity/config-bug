'use strict';

const config = require('config')
const tmBug = require('tm-config-bug')

const exposeConfig = () => console.log(config);

if( require.main === module){
    console.log("This should be ", config)
    exposeConfig()

    console.log("This should be { level: 1} ")
    tmBug.exposeConfig()
}else{
    module.exports = {exposeConfig}
}